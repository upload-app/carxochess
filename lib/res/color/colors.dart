import 'package:flutter/material.dart';

class AppColors {
  static const Color snakeFood = Color.fromARGB(255, 221, 7, 7);
  static const Color snakeBody = Color.fromARGB(255, 0, 255, 0);
  static const Color snakeGameBackground = Color.fromARGB(255, 222, 222, 229);
}
